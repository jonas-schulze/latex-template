ROOT = main

pdf: output/$(ROOT).pdf

.PHONY: update
update:
	mkdir -p output
	pdflatex -output-directory output $(ROOT).tex

.PHONY: clean
clean:
	latexmk -c

.PHONY: output/$(ROOT).pdf
output/$(ROOT).pdf:
	latexmk -pdf $(ROOT).tex
