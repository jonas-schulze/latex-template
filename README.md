# LaTeX Template

Before using this template, make sure to

* clone [latex-preamble] into the same folder (or edit `main.tex`),
* adjust/remove `README.md`,
* rename `latex-template/` and `main.tex` and adjust `Makefile` accordingly,
* reinitialize git: `rm -rf .git && git init`.

# Directory Structure

I would recommend to create the following folders:

```
content/    for all content tex files
figures/    for actual figures or tex files containing TikZ pictures
scripts/    for Bash scripts and Jupyter Notebooks
```

Note that

```
output/               will be created automatically on builds;
                      don't use it for content!
../latex-preamble/    should contain all the shared stuff;
                      if it is located elsewhere: adjust root tex file!
```

[latex-preamble]: (https://gitlab.com/jonas-schulze/latex-preamble)
